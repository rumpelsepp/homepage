---
title: Contact
---

Hi, I'm Stefan. I like to mess with Linux, network security, and free
software. You may find me in the web as `rumpelsepp`, or on:

* Matrix: @rumpelsepp:hackbrettl.de,
* [codeberg](codeberg.org/rumpelsepp),
* [Github](https://github.com/rumpelsepp), or
* [Mastodon](https://mastodon.social/@rumpelsepp).

You might also find me as `Steff` who is playing the guitar on [youtube](https://www.youtube.com/channel/UCwEd6ddfgsjP_naRbS1q9JA) or in my [band](https://www.youtube.com/channel/UC_f3A1kUkOkQzo7GXy6x-Hg).

Over the years I experimented a lot with mail servers and DNS, so there are a few email addresses lurking around in the internet:

* stefan.tatschner@gmail.com
* stefan@sevenbyte.org
* stefan.tatschner@mailbox.org (<- used for everything „serious“)
* stefan@rumpelsepp.org (<- everything else; open source stuff)

Everything is collected in a single mailbox, so it doesn't matter that much which one you choose.

## GPG Key

TODO. :D

My GPG key is available via [WKD](https://rumpelsepp.org/.well-known/openpgpkey/hu/7cr8xbb43oh9t55xqjw3g9xq1cqdqmjd)[^1].

```
$ gpg --locate-key stefan@rumpelsepp.org
```

[^1]: https://datatracker.ietf.org/doc/draft-koch-openpgp-webkey-service/
