## E-Mail

* [RFC2045](https://tools.ietf.org/html/rfc2045) -- Multipurpose Internet Mail Extensions (MIME) Part One: Format of Internet Message Bodies
* [RFC2046](https://tools.ietf.org/html/rfc2046) -- Multipurpose Internet Mail Extensions (MIME) Part Two: Media Types
* [RFC2047](https://tools.ietf.org/html/rfc2047) -- MIME (Multipurpose Internet Mail Extensions) Part Three: Message Header Extensions for Non-ASCII Text
* [RFC2183](https://tools.ietf.org/html/rfc2183) -- Communicating Presentation Information in Internet Messages: The Content-Disposition Header Field
* [RFC5321](https://tools.ietf.org/html/rfc5321) -- Simple Mail Transfer Protocol
* [RFC5322](https://tools.ietf.org/html/rfc5322) -- Internet Message Format

## IPv6

* [RFC4193](https://tools.ietf.org/html/rfc4193) -- Unique Local IPv6 Unicast Addresses
* [RFC4862](https://tools.ietf.org/html/rfc4862) -- IPv6 Stateless Address Autoconfiguration
* [RFC6890](https://tools.ietf.org/html/rfc6890) -- Special-Purpose IP Address Registries
* [RFC7078](https://tools.ietf.org/html/rfc7078) -- Distributing Address Selection Policy Using DHCPv6
* [RFC8028](https://tools.ietf.org/html/rfc8028) -- First-Hop Router Selection by Hosts in a Multi-Prefix Network
* [RFC8043](https://tools.ietf.org/html/rfc8043) -- Source-Address-Dependent Routing and Source Address Selection for IPv6 Hosts: Overview of the Problem Space
* [RFC8415](https://tools.ietf.org/html/rfc8415) -- Dynamic Host Configuration Protocol for IPv6 (DHCPv6)

## SCTP

* [RFC4960](https://tools.ietf.org/html/rfc4960) -- Stream Control Transmission Protocol
* [RFC6083](https://tools.ietf.org/html/rfc6083) -- Datagram Transport Layer Security (DTLS) for Stream Control Transmission Protocol (SCTP)
* [RFC6096](https://tools.ietf.org/html/rfc6096) -- Stream Control Transmission Protocol (SCTP) Chunk Flags Registration
* [RFC6458](https://tools.ietf.org/html/rfc6458) -- Sockets API Extensions for the Stream Control Transmission Protocol (SCTP)
* [RFC7053](https://tools.ietf.org/html/rfc7053) -- SACK-IMMEDIATELY Extension for the Stream Control Transmission Protocol

## QUIC

* [Netdev 0x13 - QUIC Tutorial - YouTube](https://www.youtube.com/watch?v=CtsBawwGwns)
* [Netdev 0x13 - Accelerating QUIC via Hardware Offloads Through a Socket Interface - YouTube](https://www.youtube.com/watch?v=ald5tP2VeGk)

## TLS

* [The Illustrated TLS 1.3 Connection: Every Byte Explained](https://tls13.ulfheim.net)
