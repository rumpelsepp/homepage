---
title: Stuff
---

## Side Projects

On this page my mentionable side projects are listed.
For my other stuff, check out my [codeberg](https://codeberg.org/rumpelsepp) account!

### mnotify

A matrix cli client.

* codeberg: https://codeberg.org/rumpelsepp/mnotify

### opennetzteil

A [specification](https://codeberg.org/rumpelsepp/opennetzteil/src/branch/master/man/netzteil-http.7.adoc) for a platform-agnostic http interface to remote control power supplies.
A reference implementation of a server and a client in Go is included.

* codeberg: https://codeberg.org/rumpelsepp/opennetzteil

### /binaries

* [Blogpost]({{< ref "2020-03-20-static-binaries-for-common-tools.md" >}})
* URL: https://rumpelsepp.org/binaries/
* Source: https://codeberg.org/rumpelsepp/binaries

## Work

At work I published a few projects.

* [PENLog](https://github.com/Fraunhofer-AISEC/penlog) provides a specification, library, and tooling for simple machine readable logging
* [frankencert](https://github.com/Fraunhofer-AISEC/frankencert) - Adversarial Testing of Certificate Validation in SSL/TLS Implementations

## Random

* [Bookmarks]({{< ref "bookmarks" >}})
* [Networking]({{< ref "networking" >}})
